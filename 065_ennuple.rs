// Questo programma mostra come immagazzinare dati di tipo differente
// in una variabile.

fn main() {
	let dati = (10000000, 183.19, 'Q');
	let copia_dei_dati = dati;

	print!("{} {} {}", dati.0, copia_dei_dati.1, dati.2);
}
