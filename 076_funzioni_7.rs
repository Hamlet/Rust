// Questo programma mostra come definire e richiamare delle funzioni.

fn raddoppia(x: f64) -> f64 { x * 2. }

fn main() {
	print!("{}", raddoppia(17.3));
}