// Questo programma mostra come sia possibile usare la lineetta "_"
// nei numeri per migliorarne la leggibilità.

fn main() {
	let esadecimale = 0x_00ff_f7a3;
	let decimale = 1_234_567;
	let ottale = 0o_777_205_162;
	let binario = 0b_0110_1001_1111_0001;

	print!("{} {} {} {}", esadecimale, decimale, ottale, binario);
}
