// Questo programma mostra come sia possibile utilizzare tipi di dato
// a numero intero "unsigned", cioè senza segno "-", ovvero solo positivi.

fn main() {
	let a: u8 = 5; // Numero intero a 8bit senza segno -.
	let b: u16 = 5; // Numero intero a 16bit senza segno -.
	let c: u32 = 5; // Numero intero a 32bit senza segno -.
	let d: u64 = 5; // Numero intero a 64bit senza segno -.

	print!("{} {} {} {}", a, b, c, d);
}
