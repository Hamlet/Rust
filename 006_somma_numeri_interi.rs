// Questo programma mostra a video il risultato della somma di
// due numeri interi, usando due metodi differenti.

fn main() {
	print!("La somma è: {}.\n", 80 + 34);
	print!("{} + {} = {}.\n", 34, 80, 80 + 34);
}
