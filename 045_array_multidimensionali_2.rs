// Questo programma mostra come ottenere le dimensioni di un array
// multidimensionale.

fn main() {
	let x = [[[[0; 4]; 6]; 8]; 15];
	print!("{}, {}, {}, {} ",
		x.len(), x[0].len(), x[0][0].len(), x[0][0][0].len());
}
