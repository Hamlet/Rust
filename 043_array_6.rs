// Questo programma mostra come scandire gli elementi di un array,
// prima assegnandogli dei valori e poi stampando i valori stessi.

fn main() {
	let mut fib = [1; 15]; // Indici da 0 a 14 con valore 1.

	for i in 2..fib.len() {
		fib[i] = fib[i - 2] + fib[i - 1]; // Sequenza di Fibonacci.
	}

	for i in 0..fib.len() {
		print!("{}, ", fib[i]);
	}
}
