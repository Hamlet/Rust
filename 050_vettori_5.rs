// Questo programma mostra come modificare un vettore inserendo e togliendo
// degli elementi.

fn main() {
	let mut x = vec!["Questa", "è", "una", "frase"];

	x.insert(1, "riga");
	x.insert(2, "contiene");
	x.remove(3);
	x.push("lunga"); // Inserisce in fondo al vettore.
	x.pop(); // Toglie dal fondo del vettore.

	for i in 0..x.len() {
		print!("{} ", x[i]);
	}
}
