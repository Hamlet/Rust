// Questo programma stampa i quadrati inferiori a 200 usando un
// ciclo infinito ("loop").
// Il compilatore avviserà di usare "loop" al posto di "while true".

fn main() {
	let mut i = 1;

	while true {
		let ii = i * i;

		if ii >= 200 { break; } // Interrompi il ciclo
		print!("{} ", ii);
		i += 1;
	}
}
