// Questo programma mostra come sia possibile determinare la dimensione
// delle variabili usate.

fn main() {
	let a: i8 = 5; // numero intero 8 bit con segno
	let b: i16 = 5; // numero intero 16 bit con segno
	let c: i32 = 5; // numero intero 32 bit con segno
	let d: i64 = 5; // numero intero 64 bit con segno

	print!("{} {} {} {}", a, b, c, d);
}
