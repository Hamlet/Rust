// Questo programma mostra come sia possibile eseguire operazioni
// matematiche su variabili dello stesso tipo.
// Invece non è possibile tra variabili di tipo diverso.

fn main() {
	let a: i16 = 5;
	let b: i16 = 18;
	let c: i32 = 5;
	let d: i32 = 18;
	let e: i64 = 5;
	let f: i64 = 18;

	print!("{} ", a + b); // Somma tra interi a 16 bit
	print!("{} ", c + d); // Somma tra interi a 32 bit
	print!("{}", e + f); // Somma tra interi a 64 bit
}
