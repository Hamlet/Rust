// Questo programma mostra come modificare gli elementi contenuti
// in un array mutabile.

fn main() {
	let mut x = ["Questa", "è", "una", "frase"];

	x[2] = "una bella";

	print!("{} {} {} {}.", x[0], x[1], x[2], x[3]);
}
