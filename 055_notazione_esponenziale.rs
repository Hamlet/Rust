// Questo programma mostra come sia possibile scrivere numeri
// letterali a virgola mobile usando la notazione esponenziale.

fn main() {
	let mille = 1e3; // "1 per 10 elevato alla terza"
	let milione = 1e6; // "1 per 10 elevato alla sesta"
	let tredici_miliardi_e_mezzo = 13.5e9; // "13,5 per 10 elevato alla nona"
	let dodici_milionesimi = 12e-6; // "12 per 10 elevato alla meno 6"

	print!("{}\n{}\n{}\n{}\n", mille, milione,
		tredici_miliardi_e_mezzo, dodici_milionesimi);
}
