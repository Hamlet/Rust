// Questo programma mostra a video il valore di una variabile mutabile
// inizializzata con un numero, e poi glie ne viene assegnato un altro.

fn main() {
	let mut numero = 12; // Dichiarazione e inizializzazione
	print!("{}", numero);
	numero = 53; // Assegnamento
	print!(" {}\n", numero);
}
