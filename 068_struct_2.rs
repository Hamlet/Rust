// Questo programma mostra come usare le "struct", cioè strutture,
// per immagazzinare tipi di dati differenti.

fn main() {
	struct VariDati {
		intero: i32,
		frazione: f32,
	};

	let mut dati = VariDati {
		intero: 10,
		frazione: 183.19,
	};

	dati.frazione = 8.2;

	print!("{} {}", dati.intero, dati.frazione);
}
