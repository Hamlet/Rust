// Questo programma visualizza a video i quadrati dei numeri interi
// da 1 a 50, che non siano divisibili per 3. I quadrati non devono
// essere superiori a 400.

fn main() {
	let mut i = 0;

	while i < 50 {
		i += 1;
		if i % 3 == 0 { continue; } // Salta l'iterazione
		if i * i > 400 { break; } // Interrompi il ciclo
		print!("{} ", i * i);
	}
}
