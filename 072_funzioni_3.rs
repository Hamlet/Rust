// Questo programma mostra come definire e richiamare delle funzioni.

fn main() {
	{
		fn f() { print!("a"); }
		f(); f();
	}

	{
		fn f() { print!("b"); }
		f();
	}
}
