// Questo programma mostra come usare le enumerazioni.

fn main() {
	enum Continente {
		Europa,
		Asia,
		Africa,
		America,
		Oceania
	}

	let contin = Continente::Asia;

	match contin {
		Continente::Europa => print!("E"),
		Continente::Asia => print!("As"),
		Continente::Africa => print!("Af"),
		Continente::America => print!("Am"),
		Continente::Oceania => print!("O"),
	}
}
