// Questo programma mostra come creare un array di dimensione specifica
// inizializzandone gli elementi con un valore di partenza.

fn main() {
	let mut x = [4.; 5000]; // Dimensione: 5000, valore: 4.0.

	x[2000] = 3.14;

	print!("{}, {}", x[1000], x[2000]); // Stampa 4, 3.14
}
