// Questo programma mostra come creare un array multidimensionale,
// assegnare un valore ad un suo elemento, e stamparne i contenuti.

fn main() {
	let mut x = [[[[23; 4]; 6]; 8]; 15];
	x[14][7][5][3] = 56;
	print!("{}, {}", x[0][0][0][0], x[14][7][5][3]);
}
