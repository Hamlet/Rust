// Questo programma mostra come definire e richiamare delle funzioni.

fn stampa_somma(addendo1: f64, addendo2: f64) {
	println!("{} + {} = {}", addendo1, addendo2, addendo1 + addendo2);
}

fn main() {
	stampa_somma(3., 5.);
	stampa_somma(3.2, 5.1);
}
