// Questo programma mostra a video il risultato di una espressione
// aritmetica avente numeri a virgola mobile.

fn main() {
	print!("{}\n", (23. - 6.) % 5. + 20. * 30. / (3. + 4.));
}
