// Questo programma mostra come definire e richiamare funzioni.

fn f() { print!("1"); }

fn main() {
	f(); // Stampa "2"

	{
		f(); // Stampa "3"
		fn f() { print!("3"); }
	}

	f(); // Stampa "2"
	fn f() { print!("2"); }
}
