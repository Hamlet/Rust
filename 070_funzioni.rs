// Questo programma mostra come definire e richiamare una funzione.

fn main() {
	fn riga() {
		println!("----------");
	}

	riga();
	riga();
	riga();
}
