// Questo programma dimostra come sia ininfluente decrementare il
// limite massimo di un ciclo "for" dopo che questo è iniziato.

fn main() {
	let mut limite = 4;

	for i in 1..limite {
		limite -= 1;
		print!("{} ", i); // Visualizza 1, 2, 3
	}

	print!("{}", limite); // Visualizza 1
}
