// Questo programma mostra come definire e richiamare delle funzioni.

fn stampa_doppio(mut x: f64) {
	x *= 2.;
	print!("{}", x);
}

fn main() {
	let x = 4.;

	stampa_doppio(x);

	print!(" {}", x);
}
