// Questo programma mostra come creare una variabile contentente una
// sequenza di dati ("array") e come accedere ai dati immagazzinati.

fn main() {
	let x = ["italiano", "Questa", "frase", "una", "in", "è"];

	print!("{} {} {} {} {} {}.", x[1], x[5], x[3], x[2], x[4], x[0]);
}
