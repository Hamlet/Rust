// Questo programma mostra come usare il tipo "const".

fn main() {
	const EUROPA: u8 = 0;
	const ASIA: u8 = 1;
	const AFRICA: u8 = 2;
	const AMERICA: u8 = 3;
	const OCEANIA: u8 = 4;

	let continente = ASIA;

	if continente == EUROPA { print!("E"); }
	else if continente == ASIA { print!("As"); }
	else if continente == AFRICA { print! ("Af"); }
	else if continente == AMERICA { print!("Am"); }
	else if continente == OCEANIA { print!("O"); }
}
