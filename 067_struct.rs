// Questo programma mostra come usare le "struct", cioè strutture,
// per immagazzinare tipi di dati differenti.

fn main() {
	struct VariDati {
		intero: i32,
		frazione: f32,
		carattere: char,
		cinque_byte: [u8; 5],
	};

	let dati = VariDati {
		intero: 10000000,
		frazione: 183.19,
		carattere: 'Q',
		cinque_byte: [9, 0, 250, 60, 200],
	};

	print!("{}, {}, {}, {}",
		dati.intero, dati.frazione, dati.carattere, dati.cinque_byte[2]);
}
