// Questo programma mostra l'utilizzo dell'istruzione condizionale
// "if", cioè "se".

fn main() {
	let n = 4;
	if n > 0 { print!("positivo\n"); }
}
