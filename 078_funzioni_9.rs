// Questo programma mostra come definire e richiamare delle funzioni.
 
fn dividi(dividendo: i32, divisore: i32) -> (i32, i32) {
	(dividendo / divisore, dividendo % divisore)
}

fn main() {
	print!("{:?}", dividi(50, 11));
}
