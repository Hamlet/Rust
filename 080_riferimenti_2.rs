// Questo programma mostra come usare i riferimenti agli indirizzi di
// memoria ed ai valori.

fn main() {
	let mut a = 10;
	let mut b = 20;
	let mut p = &mut a;

	print!("{} ", *p);

	*p += 1;
	print!("{} ", *p);

	p = &mut b;
	print!("{} ", *p);

	*p += 1;
	print!("{} ", *p);
}
