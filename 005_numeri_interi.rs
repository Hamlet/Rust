// Questo programma mostra a video dei numeri interi
// usando tre metodi differenti.

fn main() {
	print!("Numero: 140\n");		// Stringa letterale
	print!("Numero: {}\n", "140");	// Stringhe letterali
	print!("Numero: {}\n", 140);	// Stringa e intero letterali
}
