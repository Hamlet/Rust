// Questo programma mostra l'uso dei commenti.

// Questo è un commento a riga singola.

fn main() {
	print!("{}\n", 34); // Questo è un commento a latere.

	/* print!("{}\n", 80);
	Questo è un commento a riga multipla.
	*/
}
