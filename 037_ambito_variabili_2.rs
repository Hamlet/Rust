// Questo programma dimostra come l'ambito delle variabili dipenda
// dal blocco in cui sono dichiarate.

fn main() {
	let mut _i = 1;

	if true { let _i = 2; } // Crea e distrugge una seconda variabile.

	print!("{} ", _i); // Stampa il valore della prima variabile.

	while _i > 0 { _i -= 1; let _i = 5; }
	// Decrementa il valore della prima variabile e crea e distrugge
	// una seconda variabile.

	print!("{} ", _i); // Stampa il valore della prima variabile.
}
