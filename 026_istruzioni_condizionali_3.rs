// Questo programma mostra l'utilizzo delle istruzioni condizionali
// "if" cioè "se", e "else" cioè "altrimenti", in versione nidificata.

fn main() {
	let n = 4;

	if n > 1000 {
		print!("grande\n");
	}
	else {
		if n > 0 {
			print!("piccolo\n");
		}
		else {
			if n < 0 {
				print!("negativo\n");
			}
			else {
				print!("né positivo né negativo\n");
			}
		}
	}
}
