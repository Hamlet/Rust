// Questo programma stampa a video la frase "Ciao, mondo!"
// combinando due stringhe letterali.

fn main() {
	print!("{}, {}!", "Ciao", "mondo");
}
