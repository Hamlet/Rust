// Questo programma mostra l'utilizzo delle istruzioni condizionali
// "if", cioè "se", e "else", cioè "altrimenti".

fn main() {
	let n = 0;

	if n > 0 {
		print!("numero");
		println!(" positivo");
	}
	else {
		println!("non positivo");
	}
}
