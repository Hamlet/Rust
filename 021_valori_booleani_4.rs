// Questo programma mostra i valori booleani ottenuti utilizzando
// i connettori logici nelle espressioni.

fn main() {
	let vero = true;
	let falso = false;

	// Operatore ! "not", cioè "non"
	println!("{} {}", ! vero, ! falso); // false true

	// Operatore && "and", cioè "e"
	println!("{} {} {} {}", falso && falso, falso && vero,
		vero && falso, vero && vero); // false false false true

	// Operatore || "or", cioè "o"
	println!("{} {} {} {}", falso || falso, falso || vero,
		vero || falso, vero || vero); // false true true true
}
