// Questo programma mostra a video la somma tra due numeri interi
// letterali facendo uso di variabili immutabili.

fn main() {
	let numero = 12;		// Dichiarazione e assegnamento.
	let altro_numero = 53;	// Dichiarazione e assegnamento.
	print!("{}\n", numero + altro_numero);
}
