// Questo programma mostra a video i valori booleani ottenuti da
// espressioni relazionali.

fn main() {
	let vero = 5 > 2;
	let falso = -12.3 >= 10.;
	print!("{} {} {}\n", vero, falso, -50 < 6);
}
