// Questo programma mostra l'utilizzo degli operatori aritmetici
// di assegnamento: +=, -=, *=, /=.

fn main() {
	let mut a = 12;
	println!("{}", a); // 12

	a += 1;
	println!("{}", a); // 13

	a -= 4;
	println!("{}", a); // 9

	a *= 7;
	println!("{}", a); // 63

	a /= 6;
	println!("{}", a); // 10
}
