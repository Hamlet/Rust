// Questo programma mostra a video il risultato di una
// operazione aritmetica avente diversi operatori.

fn main() {
	print!("{}\n", (23 - 6) % 5 + 20 * 30 / (3 + 4));
}
