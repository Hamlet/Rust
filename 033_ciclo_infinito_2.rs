// Questo programma stampa i quadrati inferiori a 200 usando un
// ciclo infinito ("loop").

fn main() {
	let mut i = 1;

	loop {
		let ii = i * i;

		if ii >= 200 { break; } // Interrompi il ciclo
		print!("{} ", ii);
		i += 1;
	}
}
