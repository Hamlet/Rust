// Questo programma mostra a video la somma di due numeri frazionari,
// cioè a virgola mobile.

fn main() {
	print!("La somma è {}.\n", 80.3 + 34.8); // 115.1
	print!("La somma è {}.\n", 80.3 + 34.9); // 115.19999999999999
}
