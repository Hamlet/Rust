// Questo programma mostra come usare le "struct ennuple" per immagazzinare
// tipi di dato differenti.

fn main() {
	struct VariDati (
		i32,
		f32,
		char,
		[u8; 5],
	);

	let dati = VariDati (
		10000000,
		183.19,
		'Q',
		[9, 0, 250, 60, 200],
	);

	print!("{} {} {} {}", dati.0, dati.1, dati.2, dati.3[2]);
}
