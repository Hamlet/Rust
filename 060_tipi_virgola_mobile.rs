// Questo programma mostra come specificare il tipo di una variabile
// che dovrà contente un numero frazionario.

fn main() {
	let a: f64 = 4.6; // Numero frazionario a 64bit.
	let b: f32 = 3.91; // Numero frazionario a 32bit.

	print!("{} {}", a, b);
}
