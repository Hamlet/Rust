// Questo programma mostra come creare e modificare un vettore in
// fase di esecuzione, sia per i valori che per la dimensione.

fn main() {
	let lunghezza = 5000;
	let mut y = vec![4.; lunghezza]; // Dimensione: da 0 a 4999.

	y[6] = 3.14;
	y.push(4.89); // Accoda un nuovo elemento in posizione 5000.

	println!("{}, {}, {}", y[6], y[4999], y[5000]);
}
