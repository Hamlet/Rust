// Questo programma mostra come usare una funzione contenuta nella
// libreria standard, utilizzando due metodi diversi:
// in stile procedurale, e in stile object-oriented.

fn main() {
	print!("{} {}\n", str::len("abcde"), "abcde".len());
	// Mostra il numero di byte contenuti nella stringa.
}
