// Questo programma mostra come creare e modificare un vettore in fase
// di esecuzione, sia per i contenuti che per la dimensione.

fn main() {
	let mut x = vec!["Questa", "è"];
	print!("{}", x.len());

	x.push("una"); // Accoda il nuovo elemento
	print!(" {}", x.len());

	x.push("frase"); // Accoda il nuovo elemento
	print!(" {}", x.len());

	x[0] = "Quella"; // Modifica il primo elemento
	for i in 0..x.len() {
		print!(" {}", x[i]);
	}
}
