// Questo programma mostra come è possibile vincolare esplicitamente un
// numero a un formato, usando i metodi disponibili.

fn main() {
	let _a: i16 = -150; // Numero intero a 16bit.
	let _b = -150 as i16; // Numero intero a 16bit.
	let _c = -150 + _b - _b;
	// Numero intero a 16 bit, dedotto dall'espressione "_b - _b".
	let _d = -150i16; // Numero intero a 16bit.

	print!("{} {} {} {}", _a, _b, _c, _d);
}
