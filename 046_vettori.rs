// Questo programma mostra come creare un vettore e calcolarne la
// dimensione.

fn main() {
	let x = vec!["Questa", "è"];
	print!("{} {}. Lunghezza: {}.", x[0], x[1], x.len());
}
