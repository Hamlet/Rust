// Questo programma dimostra come l'ambito delle variabili dipenda
// dal blocco in cui sono dichiarate.

fn main() {
	let i = 10;

	{
		let i = 4; // Oscura la prima "i".

		print!("{} ", i); // Stampa 4

	} // Fine dell'ambito della seconda "i".

	print!("{}", i); // Stampa 10

} // Fine dell'ambito della prima "i".
