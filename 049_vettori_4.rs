// Questo programma mostra come sia possibile assegnare a un vettore
// un altro vettore di lunghezza diversa, ma dello stesso tipo.

fn main() {
	let mut _x = vec!["a", "b", "c"]; // Lunghezza 3, tipo stringa
	print!("Lunghezza: {} - {} {} {}\n", _x.len(), _x[0], _x[1], _x[2]);

	_x = vec!["X", "Y"]; // Lunghezza 2, tipo stringa
	print!("Lunghezza: {} - {} {}\n", _x.len(), _x[0], _x[1]);
}
