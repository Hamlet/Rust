// Questo programma mostra a video la somma tra due numeri interi
// usando più istruzioni.

fn main() {
	print!("{} + ", 80);		// Prima istruzione
	print!("{} =", 34);			// Seconda istruzione
	print!(" {}\n", 80 + 34);	// Terza istruzione
}
