// Questo programma mostra a video il valore booleano di due variabili
// immutabili.

fn main() {
	let vero = true;
	let falso = false;
	print!("{} {}\n", vero, falso);
}
