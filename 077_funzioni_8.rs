// Questo programma mostra come definire e richiamare delle funzioni.

fn f(x: f64) -> f64 {
	if x <= 0. { return 0.; }
	x + 3.
}

fn main() {
	print!("{}", f(10.));
}
