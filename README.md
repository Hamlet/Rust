Esercizi di programmazione in linguaggio [Rust][1]  
Basati sul libro "[Beginning Rust: From Novice to Professional][2]"  
di [Carlo Milanesi][3]

Licenza: [CC 1.0 Universal][4] o successiva.


[1]: https://it.wikipedia.org/wiki/Rust_(linguaggio_di_programmazione)
[2]: https://www.amazon.com/Beginning-Rust-Professional-Carlo-Milanesi/dp/1484234677
[3]: https://www.amazon.com/Carlo-Milanesi/e/B08B7QVVBJ/ref=dp_byline_cont_book_1
[4]: https://creativecommons.org/publicdomain/zero/1.0/deed.it
