// Questo programma visualizza a video i quadrati dei numeri da 1 a 10.

fn main() {
	let mut i = 1;

	while i <= 10 {
		print!("{} ", i * i);
		i += 1;
	}
}
