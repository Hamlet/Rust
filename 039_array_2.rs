// Questo programma mostra come determinare il numero di elementi
// presenti in un array.

fn main() {
	let a = [true, false]; // Array di valori booleani.
	let b = [1, 2, 3, 4, 5]; // Array di numeri interi.

	print!("{}, {}.", a.len(), b.len());
}
