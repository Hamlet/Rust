// Questo programma mostra l'utilizzo delle istruzioni condizionali
// "if" cioè "se", e "else" cioè "altrimenti", in versione nidificata.

fn main() {
	let n = 4;

	print!("{}\n",
		if n > 1000 {
			"grande"
		}
		else if n > 0 {
			"piccolo"
		}
		else if n < 0 {
			"negativo"
		}
		else {
			"né positivo né negativo"
		}
	);
}
