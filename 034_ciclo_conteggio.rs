// Questo programma visualizza a video i quadrati dei numeri interi
// da 1 a 10.

fn main() {
	for i in 1..11 { // Il primo numero è incluso, il secondo no.
		print!("{} ", i * i);
	}
}
