// Questo programma mostra l'utilizzo della parola chiave "usize",
// cioé "unsigned size": dimensione senza segno -.

fn main() {
	let arr = [11, 22, 33];
	let i: usize = 2;
	// Il tipo sarà u32 o u64 a seconda del sistema operativo per
	// cui verrà generato il codice eseguibile.

	print!("{}", arr[i]);
}
