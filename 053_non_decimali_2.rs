// Questo programma mostra come l'uso di numeri non decimali sia
// comunque un uso di numeri interi.

fn main() {
	let esadecimale = 0x10; // 16
	let ottale = 0o10; // 8
	let binario = 0b10; // 2
	let mut n = 10; // Variabile mutabile di tipo numero intero

	print!("{} ", n);

	n = esadecimale;
	print!("{} ", n);

	n = ottale;
	print!("{} ", n);

	n = binario;
	print!("{} ", n);
}
