// Questo programma mostra come visualizzare gli elementi di un
// array e di un vettore senza ricorrere a un ciclo for.

fn main() {
	print!("{:?} {:?}", [1, 2, 3], vec![4, 5]);
}
