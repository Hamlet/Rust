// Questo programma mostra a video i valori booleani ottenuti
// tramite gli operatori relazionali applicati alle stringhe letterali.

fn main() {
	print!("{} {} {}\n", "abc" < "abcd", "ab" < "ac", "A" < "a");
	// NOTA: in questo caso il simbolo "<" si legge "precede".
}
