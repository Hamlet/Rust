// Questo programma mostra come usare i riferimenti agli indirizzi di
// memoria ed ai valori.

fn main() {
	let a = 15;
	let rif_a = &a;

	print!("{} {} {}", a, *rif_a, rif_a);
}
