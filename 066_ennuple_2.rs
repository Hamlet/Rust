// Questo programma mostra come immagazzinare e cambiare tipi di dato
// differenti in una variabile.

fn main() {
	let mut dati = (10000000, 183.19, 'Q');

	dati.0 = -5;
	dati.2 = 'x';
	print!("{} {} {}", dati.0, dati.1, dati.2);
}
