// Questo programma mostra come usare le basi numeriche non decimali.

fn main() {
	let esadecimale = 0x10; // 16
	let decimale = 10;
	let ottale = 0o10; // 8
	let binario = 0b10; // 2

	print!("{} {} {} {}", esadecimale, decimale, ottale, binario);
}
